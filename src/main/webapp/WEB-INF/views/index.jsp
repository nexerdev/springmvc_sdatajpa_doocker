<%@page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page isELIgnored="false" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="ISO-8859-1">
    <title>hello</title>
</head>
<body>

<p>All books</p>
<a href="add">Add books from folder RES</a>
<br>
<table border="1">
    <tr>
        <th>ID</th>
        <th>Author first name</th>
        <th>Author last name</th>
        <th>Title</th>
        <th>Date</th>
        <th>Lang</th>
        <th>Custom info</th>
        <th>Annotation</th>
        <th>Del</th>
    </tr>
    <c:forEach items="${allBooks }" var="book">
        <tr>
            <th>${ book.id }</th>
            <th>${ book.authorFirstName }</th>
            <th>${ book.authorLastName }</th>
            <th>${ book.bookTitle }</th>
            <th>${ book.date }</th>
            <th>${ book.lang }</th>
            <th>${ book.customInfo }</th>
            <th>${ book.annotation }</th>
            <th><a href="delete-${book.id}">del</a></th>
        </tr>
    </c:forEach>
</table>

<br>
<p>${ book }</p>
</body>
</html>