<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"  xmlns:ns1="http://www.gribuser.ru/xml/fictionbook/2.0" exclude-result-prefixes="ns1">
    <xsl:output method="xml" indent="yes" />

    <xsl:template match="/">
            <book>
                <author-first-name>
                    <xsl:value-of select="/ns1:FictionBook/ns1:description/ns1:title-info/ns1:author/ns1:first-name"/>
                </author-first-name>
                <author-last-name>
                    <xsl:value-of select="/ns1:FictionBook/ns1:description/ns1:title-info/ns1:author/ns1:last-name"/>
                </author-last-name>
                <book-title>
                    <xsl:value-of select="/ns1:FictionBook/ns1:description/ns1:title-info/ns1:book-title"/>
                </book-title>
                <date>
                    <xsl:value-of select="/ns1:FictionBook/ns1:description/ns1:title-info/ns1:date"/>
                </date>
                <lang>
                    <xsl:value-of select="/ns1:FictionBook/ns1:description/ns1:title-info/ns1:lang"/>
                </lang>
                <custom-info>
                    <xsl:value-of select="/ns1:FictionBook/ns1:description/ns1:custom-info"/>
                </custom-info>
                <annotation>
                    <xsl:value-of select="/ns1:FictionBook/ns1:description/ns1:title-info/ns1:annotation"/>
                </annotation>
            </book>
    </xsl:template>
</xsl:stylesheet>