package by.epam.gomel.preproduction.kholad.app;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.InputStream;

public class XslConvertot {
    public String xmlToXml(String xmlFile, String xslFile)  {
        try{
            InputStream xml = new FileInputStream(xmlFile);
            InputStream xsl = new FileInputStream(xslFile);
            StreamSource xmlSource = new StreamSource(xml);
            StreamSource stylesource = new StreamSource(xsl);

            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            StreamResult xmlOutput = new StreamResult(bos);
            Transformer transformer = TransformerFactory.newInstance().newTransformer(stylesource);
            transformer.transform(xmlSource, xmlOutput);
            return bos.toString();
        }catch (Exception e){
            System.err.println(e);
        }
        return null;
    }
}
