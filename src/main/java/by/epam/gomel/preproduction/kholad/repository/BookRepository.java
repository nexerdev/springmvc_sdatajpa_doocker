package by.epam.gomel.preproduction.kholad.repository;

import by.epam.gomel.preproduction.kholad.entity.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface BookRepository extends JpaRepository<Book, Integer> {

}