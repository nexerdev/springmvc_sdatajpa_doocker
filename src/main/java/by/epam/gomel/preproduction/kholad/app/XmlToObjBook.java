package by.epam.gomel.preproduction.kholad.app;

import by.epam.gomel.preproduction.kholad.entity.Book;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public class XmlToObjBook {
    public static Book fromXmlToObject(String filePath) {
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(Book.class);
            Unmarshaller un = jaxbContext.createUnmarshaller();
            Book book = (Book) un.unmarshal(new File(filePath));


            String bookTitle = book.getBookTitle();
            return book ;
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return null;
    }
}