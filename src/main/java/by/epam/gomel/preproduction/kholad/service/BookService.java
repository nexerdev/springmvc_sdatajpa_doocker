package by.epam.gomel.preproduction.kholad.service;

import by.epam.gomel.preproduction.kholad.entity.Book;
import by.epam.gomel.preproduction.kholad.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
@Transactional
public class BookService {
    @Autowired
    BookRepository bookRepository;

    public List<Book> allBooks(){
        return bookRepository.findAll();
    }
    public void saveBook(Book book){
        bookRepository.save(book);
    }
    public void deleteBook(Integer bookId){
        bookRepository.deleteById(bookId);
    }
}
