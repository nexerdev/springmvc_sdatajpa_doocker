package by.epam.gomel.preproduction.kholad.controller;

import by.epam.gomel.preproduction.kholad.app.BooksFromXml;
import by.epam.gomel.preproduction.kholad.entity.Book;
import by.epam.gomel.preproduction.kholad.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Controller
public class HelloController {
    @Autowired
    BookService bookService;

    @GetMapping( "/")
    public String homePage(Model model) {
        model.addAttribute("error","123");
        model.addAttribute("allBooks",bookService.allBooks());
        return "index";
    }

    @GetMapping(value = "/add")
    public String addBooksFromFolder () {
        BooksFromXml bookFromXml = new BooksFromXml();
        List<Book> listBooks =  bookFromXml.getListBooks();
        listBooks.stream().forEach(x -> bookService.saveBook(x));
        return "redirect:/";
    }

    @GetMapping("/delete-{bookID}")
    public String deleteBook(@PathVariable("bookID") Integer bookID) {

        bookService.deleteBook(bookID);
        return "redirect:/";
    }
}
