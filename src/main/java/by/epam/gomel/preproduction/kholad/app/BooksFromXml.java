package by.epam.gomel.preproduction.kholad.app;

import by.epam.gomel.preproduction.kholad.entity.Book;

import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;

public class BooksFromXml {

    public List<Book> getListBooks() {
        List<Book> listBooks = new ArrayList<>();



        ClassLoader classLoader = getClass().getClassLoader();
        String pathBooksClassLoader= classLoader.getResource("/books").getPath();
        String pathBooks = pathBooksClassLoader.substring(1).replace("/", "\\\\").replace("%20"," ");

        CheckAvailabilityFb2InFolder checkAvailabilityFb2InFolder = new CheckAvailabilityFb2InFolder();

        List<String> listFb2Name = checkAvailabilityFb2InFolder.getListNameFb2(pathBooks);

        XslConvertot convertot = new XslConvertot();
        final String xsl = pathBooks + "book_template.xsl";
        final String resultBooksFormated = pathBooks + "resultBooksFormated.xml";


        for (int i = 0; i < listFb2Name.size(); i++) {
            final String bookFb2 = pathBooks  + listFb2Name.get(i);

            try (FileWriter writer = new FileWriter(resultBooksFormated)) {
                writer.write(convertot.xmlToXml(bookFb2, xsl));
                writer.flush();
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }

            Book book = XmlToObjBook.fromXmlToObject(resultBooksFormated);

            if (book != null) {
                listBooks.add(book);
            }
        }

        return listBooks;
    }
}
