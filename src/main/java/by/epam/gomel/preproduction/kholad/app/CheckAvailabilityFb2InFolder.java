package by.epam.gomel.preproduction.kholad.app;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.List;

public class CheckAvailabilityFb2InFolder {
    public List<String> getListNameFb2(String dirWithBooks){
        List<String> nameBooks = new ArrayList<>();


        File dir = new File(dirWithBooks);
        File[] listOfFiles = dir.listFiles(new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return name.toLowerCase().endsWith(".fb2");
            }
        });

        for (File file : listOfFiles) {
            nameBooks.add(file.getName());
        }
        return nameBooks;
    }
}
