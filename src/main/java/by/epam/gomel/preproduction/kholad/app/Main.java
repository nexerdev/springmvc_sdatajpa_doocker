package by.epam.gomel.preproduction.kholad.app;

import by.epam.gomel.preproduction.kholad.entity.Book;

import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
      Res res = new Res();
        List<Book> listBooks = res.getListBooks();
    }
}

class Res {
    public List<Book> getListBooks() {
        List<Book> listBooks = new ArrayList<>();
        CheckAvailabilityFb2InFolder checkAvailabilityFb2InFolder = new CheckAvailabilityFb2InFolder();
        List<String> listFb2 = checkAvailabilityFb2InFolder.getListNameFb2("src\\main\\resources\\books");
        System.out.println(listFb2);

        XslConvertot convertot = new XslConvertot();
        final String xsl = "src\\main\\resources\\books\\book_template.xsl";
        final String resultBooksFormated = "src\\main\\resources\\books\\resultBooksFormated.xml";

        for (int i = 0; i < listFb2.size(); i++) {
            final String bookFb2 = "src\\main\\resources\\books\\" + listFb2.get(i);

            try (FileWriter writer = new FileWriter(resultBooksFormated)) {
                writer.write(convertot.xmlToXml(bookFb2, xsl));
                writer.flush();
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }

            Book book = XmlToObjBook.fromXmlToObject(resultBooksFormated);

            if (book != null) {
                System.out.println(book.toString());
                listBooks.add(book);
            }
        }
        return listBooks;
    }    
}
