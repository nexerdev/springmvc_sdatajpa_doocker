package by.epam.gomel.preproduction.kholad.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan(basePackages = {"by.epam.gomel.preproduction.kholad.service","by.epam.gomel.preproduction.kholad.config",})
public class RootConfig {

}