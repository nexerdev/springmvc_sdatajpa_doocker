package by.epam.gomel.preproduction.kholad.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement(name = "book")
public class Book {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Integer id;
    private String authorFirstName;
    private String authorLastName;
    private String bookTitle;
    private String date;
    private String lang;
    private String customInfo;
    @Column(columnDefinition = "TEXT")
    private String annotation;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAuthorFirstName() {
        return authorFirstName;
    }
    @XmlElement(name = "author-first-name")
    public void setAuthorFirstName(String authorFirstName) {
        this.authorFirstName = authorFirstName;
    }

    public String getAuthorLastName() {
        return authorLastName;
    }

    @XmlElement(name = "author-last-name")
    public void setAuthorLastName(String authorLastName) {
        this.authorLastName = authorLastName;
    }

    public String getBookTitle() {
        return bookTitle;
    }
    @XmlElement(name = "book-title")
    public void setBookTitle(String bookTitle) {
        this.bookTitle = bookTitle;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getCustomInfo() {
        return customInfo;
    }
    @XmlElement(name = "custom-info")
    public void setCustomInfo(String customInfo) {
        this.customInfo = customInfo;
    }

    public String getAnnotation() {
        return annotation;
    }

    public void setAnnotation(String annotation) {
        this.annotation = annotation;
    }

    @Override
    public String toString() {
        return "Book{" + "\n" +
                "Name autohor = " + authorFirstName + "\n" +
                "Last Name author= " + authorLastName + "\n" +
                "Title = " + bookTitle + "\n" +
                "Date= " + date + "\n" +
                "Lang =" + lang + "\n" +
                "Custom info= " + customInfo + "\n" +
                "annotation = " + annotation + "\n" +
                '}';
    }
}
