FROM tomcat:9.0-jre11

MAINTAINER uladzislau

COPY target/module1.war /usr/local/tomcat/webapps/

EXPOSE 8080
CMD ["/usr/local/tomcat/bin/catalina.sh", "run"]
